import numpy as np
from pathlib import Path
from typing import List, Dict

from pcdet.datasets import DatasetTemplate


class KRadarDataset(DatasetTemplate):
    def __init__(self, dataset_cfg=None, class_names=None, training=True, root_path=None, logger=None):
        root_path = root_path if root_path is not None else Path(dataset_cfg.DATA_PATH)
        super().__init__(dataset_cfg, class_names, training, root_path, logger)
        
        self.infos: List[Dict] = list()
        # self.infos stores meta-information of every data sample of the K-Radar dataset
        # meta-information includes everything you need to read sensor data & labels from hard disk, calibration info,
        # and ego vehicle localization 

    def populate_infos(self, is_training: bool) -> None:
        """
        TODO: Minh
        """
        self.infos = list()

    def create_infos(self) -> None:
        """
        TODO: Minh

        Go through the entire dataset extract meta-info of every data sample & write them to a file for train/ test set
        """
        pass

    def sequence_get_raw_radar(self, sequence_index: int, timestamp: int) -> np.ndarray:
        """
        TODO: Song/ Dong

        Get the RAW radar identified by the timestamp{timestamp} of a sequence{sequence_index}

        Args:
            sequence_index: in {1, 2, ..., 58}
            timestamp: timestamp of the radar

        Return:
            raw_4d_radar_data: TODO what is dimension of the raw radar?
            TODO: radar is in which frame?
        """
        pass

    def sequence_get_processed_radar(self, sequence_index: int, timestamp: int) -> np.ndarray:
        """
        TODO: Song/ Dong

        Get the PROCESSED radar identified by the timestamp{timestamp} of a sequence{sequence_index}
        the term PROCESSED refers to the radar data that you use to train your RTNH.

        Args:
            sequence_index: in {1, 2, ..., 58}
            timestamp: timestamp of the radar

        Return:
            processed_4d_radar_data: TODO what is dimension of the processed radar?
            TODO: radar is in which frame?
        """
        pass

    def sequence_get_lidar_os1(self, sequence_index: int, timestamp: int) -> np.ndarray:
        """
        TODO: Song/ Dong

        Get the point cloud of the OS1-128 identified by the timestamp{timestamp} of a sequence{sequence_index}

        Args:
            sequence_index: in {1, 2, ..., 58}
            timestamp: timestamp of the radar

        Return:
            points_os1: (N, 4+C) - x, y, z, reflectant, (and C additional channels, if there is any)
            TODO: points_os1 is in which frame?
        """
        pass

    def sequence_get_lidar_os2(self, sequence_index: int, timestamp: int) -> np.ndarray:
        """
        TODO: Song/ Dong

        Get the point cloud of the OS2-64 identified by the timestamp{timestamp} of a sequence{sequence_index}

        Args:
            sequence_index: in {1, 2, ..., 58}
            timestamp: timestamp of the radar

        Return:
            points_os2: (N, 4+C) - x, y, z, reflectant, (and C additional channels, if there is any)
            TODO points_os2 is in which frame?
        """
        pass

    def sequence_get_ego_SE3_radar(self, sequence_index: int) -> np.ndarray:
        """
        TODO: Song/ Dong

        Get the rigid body transformation that transforms points in the radar frame to the ego vehicle frame
        NOTE: I use "ego vehicle frame" to the refer to a common frame attached to the ego vehicle (e.g., the GPS, 
        or OS1) which you to calibrate radar with respect to.

        Args:
            sequence_index: in {1, 2, ..., 58}

        Returns:
            ego_se3_radar: (4, 4) - homogeneous rigid boday transformation from radar frame to the ego frame        
        """
        pass

    def sequence_get_ego_SE3_os1(self, sequence_index: int) -> np.ndarray:
        """
        TODO: Song/ Dong

        Get the rigid body transformation that transforms points in the OS1 frame to the ego vehicle frame
        NOTE: I use "ego vehicle frame" to the refer to a common frame attached to the ego vehicle (e.g., the GPS, 
        or OS1) which you to calibrate OS1 with respect to.

        Args:
            sequence_index: in {1, 2, ..., 58}

        Returns:
            ego_se3_os1: (4, 4) - homogeneous rigid boday transformation from radar frame to the ego frame        
        """
        pass

    def sequence_get_ego_SE3_os2(self, sequence_index: int) -> np.ndarray:
        """
        TODO: Song/ Dong

        Get the rigid body transformation that transforms points in the OS2 frame to the ego vehicle frame
        NOTE: I use "ego vehicle frame" to the refer to a common frame attached to the ego vehicle (e.g., the GPS, 
        or OS1) which you to calibrate OS2 with respect to.

        Args:
            sequence_index: in {1, 2, ..., 58}

        Returns:
            ego_se3_os2: (4, 4) - homogeneous rigid boday transformation from radar frame to the ego frame        
        """
        pass

    def __getitem__(self, index) -> Dict[str, np.ndarray]:
        """
        TODO: Minh
        """
        pass
    